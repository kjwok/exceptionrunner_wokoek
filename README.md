Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
	Because logging to the console only output data to the console, while logger.log can be used to debug and can be output to multiple places. Logger.log can also output custom data, you can choose to print some things and not to print others, like Levels.

1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
	

1.  What does Assertions.assertThrows do?
	It tests exception cases and throws an exception if the condition is false.

1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
		Each serializable class gets an ID that verifies if the class being used is comparable with the serialized object it's being used with.
    2.  Why do we need to override constructors?
		We override constructors because there are multiple outputs we could give depending on what is thrown, at least in the case of TimerException
    3.  Why we did not override other Exception methods?
		We don't because we are only using the InterruptedException category of Exceptions 	

1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
	It attempts to open a configuration file and configure a logger. If an exception is thrown, an error message is output to the console, otherwise, the logger logs that the open was successful to info.

1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
	The a .md (markdown) file is essentially a text file that can be used as either a README file or with any pull request's descriptions/comments. 

1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
	Test is failing because we got a NullPointerException instead of a TimerException

1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
	Because we initialized timeNow to null it doesn't get set so causes exception.

1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 

1.  Make a printScreen of your eclipse Maven test run, with console

1.  What category of Exceptions is TimerException and what is NullPointerException
	TimerException is an InterruptedException and NullPointerException is a run time exception that throws when the program attempts to use an object that had a null value.
1.  Push the updated/fixed source code to your own repository.